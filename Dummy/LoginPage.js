var Request = require("JSLib/comm.js");
var Observable = require("FuseJS/Observable");
var username = Observable("");
var password = Observable("");
var auth_success = Observable(false);
var auth_fail = Observable(false);
var start_load_animation = Observable(false);

var user_pass_color = Observable("#999");

var appDB = localStorage;

function authenticate()
{
  if(username.value && password.value)
  {
    // Set the loading animation to motion
    start_load_animation.value = true;


    // Check username and password
    if(username.value == "foo" && password.value == "fee")
    {
      appDB.setItem("user_key","K3Y");
      auth_success.value = true;
    }
    else
    {
      auth_fail.value = true;
      setTimeout(function() {
            auth_fail.value = false;
        }, 0500);
      username.value = "";
      password.value = "";
      start_load_animation.value = false;
    }

  }
  else
  {
    user_pass_color.value = "#f00";
  }

}

module.exports =
{
  user_pass_color: user_pass_color,
  start_load_animation: start_load_animation,
  username: username,
  password: password,
  authenticate: authenticate,
  auth_success: auth_success,
  auth_fail: auth_fail,
}
