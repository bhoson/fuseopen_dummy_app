var baseurl = 'http://example.com';

function request(url,requestData,successCallback=null,failureCallback=null)
{
      fetch(baseurl+url,{
       method: POST,
       headers: { "Content-type": "application/json"},
       body: JSON.stringify(requestData)
       })
      .then(function(response)
      {
        if (!response.ok)
        {
          console.log("[ JS error response ]");
          console.log(response.statusText);
          throw Error(response.statusText);
        }
          return response.json();
      })
     .then(function(response)
     {
       if(successCallback)
       {
         successCallback(response);
       }
     })
     .catch(function(error)
     {
       if(failureCallback)
       {
         failureCallback(error)
       }

     });
}
