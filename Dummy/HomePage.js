function goto_first()
{
  user_router.goto("firstPage");
}

function goto_second()
{
  user_router.goto("secondPage");
}

module.exports =
{
  goto_first: goto_first,
  goto_second: goto_second,
}
