var Observable = require("FuseJS/Observable");

var Camera = _cameraView;

var FileSystem = require("FuseJS/FileSystem");

var photoPath = Observable();
var savePath = Observable(FileSystem.dataDirectory + "/" + "DummyImages/");

var photoCount = Observable(0);
var allFiles = Observable();

function _init_()
{

  FileSystem.listFiles(FileSystem.dataDirectory + "/" + "DummyImages/")
  .then(function(files)
  {
      photoCount.value = files.length;
      allFiles.clear();
      allFiles.addAll(files);

      console.log(photoCount.value);
    }, function(error) {
        console.log("Unable to list files of directory: " + error);
    });


}

function capture_photo()
{
    Camera.capturePhoto(640,480)
        .then(function(photo)
        {
            photo.save()
                .then(function(path)
                {
                    console.log("Photo saved to: " + path);
                    photoPath.value = path;
                    photo.release();
                })
                .catch(function(error)
                {
                    console.log("Failed to save photo: " + error);
                    photo.release();
                });
        })
        .catch(function(error)
        {
            console.log("Failed to capture photo: " + error);
        });
}

function save_photo()
{
  FileSystem.move(photoPath.value,savePath.value+photoCount.value+1+".jpg");
}


module.exports =
{
  capture_photo: capture_photo,
  save_photo: save_photo,
  _init_: _init_,
  photoCount: photoCount,
  allFiles: allFiles,

}
