var Observable = require("FuseJS/Observable");
var VideoCamera = _cameraViewForVideo;
var recordingSession = null;
var FileSystem = require("FuseJS/FileSystem");
var savePath = FileSystem.dataDirectory + "/" + "DummyVideos/";
var videoCount = Observable();
var videoPath = "";
var saved = false;
var allVideoFiles = Observable();

function _init_()
{
  saved = false;
  var recordingSession = null;
  videoPath = "";

  FileSystem.listEntries(FileSystem.dataDirectory + "/" + "DummyVideos/")
  .then(function(files)
  {
      videoCount.value = files.length;
      allVideoFiles.clear();
      allVideoFiles.addAll(files);

    }, function(error) {
        console.log("Unable to list files of directory: " + error);
    });

}

function start_recording()
{

  console.log("Start record");
  VideoCamera.setCaptureMode(VideoCamera.CAPTURE_MODE_VIDEO)
    .then(function(newCaptureMode) { /* ready to record video */ })
    .catch(function(error) { /* failed */ });

  VideoCamera.startRecording()
        .then(function(session) {
            console.log("Video recording started!");
            recordingSession = session;
        })
        .catch(function(error) {
            console.log("Failed to start recording: " + error);
        });
}


function stop_recording()
{
  console.log("Stop record");
  if (recordingSession == null)
        return;

    recordingSession.stop()
        .then(function(recording) {
            videoPath = recording.filePath();
            console.log("Recording stopped, saved to: " + videoPath);
            recordingSession = null;
        })
        .catch(function(error) {
            console.log("Failed to stop recording: " + error);
            recordingSession = null;
        });
}

function save_video()
{
  console.log("Save video");
  FileSystem.move(videoPath,savePath+videoCount.value+1+".mov");
  saved = true;
}

function check_save_before_closing()
{
  if(!saved && videoPath != "")
  {
    FileSystem.delete(videoPath).then(function(){
      console.log("Removed temporary saved video");
    },function(error){
      console.log("Failed to remove temporarily saved video: "+error);
    });
  }
}


module.exports =
{
  _init_: _init_,
  start_recording: start_recording,
  stop_recording: stop_recording,
  save_video: save_video,
  allVideoFiles: allVideoFiles,
  check_save_before_closing: check_save_before_closing,

}
